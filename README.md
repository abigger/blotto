[![Netlify Status](https://api.netlify.com/api/v1/badges/e4170eed-14fb-42e4-9f8f-c91bbbd536dd/deploy-status)](https://app.netlify.com/sites/blottojs/deploys)

# Blotto

Machine learning React App to determine the best configuration for a blotto game given constraints provided by the user.

## Available Scripts

### `npm start`