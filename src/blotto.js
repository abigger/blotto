import React, { Component } from 'react';
import logo from './logo.svg';
import * as tfvis from 'ml5';

class Blotto extends Component {

    async getData() {
        const carsDataReq = await fetch('https://storage.googleapis.com/tfjs-tutorials/carsData.json');  
        const carsData = await carsDataReq.json();  
        const cleaned = carsData.map(car => ({
            mpg: car.Miles_per_Gallon,
            horsepower: car.Horsepower,
        }))
        .filter(car => (car.mpg != null && car.horsepower != null));
        
        return cleaned;
    }

    async run() {
  // Load and plot the original input data that we are going to train on.
  const data = await this.getData();
  const values = data.map(d => ({
    x: d.horsepower,
    y: d.mpg,
  }));

  tfvis.render.scatterplot(
    {name: 'Horsepower v MPG'},
    {values}, 
    {
      xLabel: 'Horsepower',
      yLabel: 'MPG',
      height: 300
    }
  );

  // More code will be added below
}
  render() {
      console.log(this.getData());
      console.log(this.run());
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default Blotto;


