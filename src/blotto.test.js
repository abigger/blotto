import React from 'react';
import ReactDOM from 'react-dom';
import Blotto from './Blotto';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Blotto />, div);
  ReactDOM.unmountComponentAtNode(div);
});