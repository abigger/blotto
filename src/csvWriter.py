# Andreas Bigger
# Write Blotto data to csv file
# 4/8/2019

from array import *
import csv


def recursiveWriter(filewriter, sumCombination, castleNum, row, iteration):
    if(castleNum == 10):
        row[iteration].append(100-sumCombination)
        filewriter.writerow(row[iteration])
        sumCombination = 0
        castleNum = 1
        iteration += 1
    else:
        if(castleNum == 1 and sumCombination == 99):
            print('1% done')
        for x in range(100-sumCombination):
            row[iteration].append(x)
            recursiveWriter(filewriter, sumCombination + x,
                            castleNum + 1, row, iteration)


# Open CSV file
with open('data.csv', 'w') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    # Write definition row
    filewriter.writerow(['Number', 'C1', 'C2', 'C3', 'C4',
                         'C5', 'C6', 'C7', 'C8', 'C9', 'C10'])

    # Algorithm
    sumCombination = 0
    castleNum = 0
    row = [[]]
    iteration = 0
    recursiveWriter(filewriter, sumCombination, castleNum, row, iteration)
