# base image
FROM node

# set working directory
RUN mkdir /blotto
WORKDIR /blotto

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /blotto/node_modules/.bin:$PATH

# install and cache app dependencies
COPY . /blotto
RUN npm install --silent
RUN npm install react-scripts -g --silent

# start app
CMD ["npm","run","start"]
